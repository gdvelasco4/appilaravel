<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = 'movies';
    protected $fillable = ['name','description', 'user_id', 'status_id'];
    protected $guarded = ['id'];

    //Relacion muchos a muchos de Movie y Category
    public function categories()
    {
    	return $this->belongsToMany('App\Models\Category');
    }
}
